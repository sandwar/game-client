using System;
using UnityEngine;
using SandWar.Unity.Tools;

namespace GameClient.UI.Notifications
{
	/// <summary>
	/// Allows to show notifications on the screen.
	/// 
	/// Fully thread safe.
	/// </summary>
	public class Notifications : Singleton<Notifications>
	{
		Internal.NotificationsBoard board;

		/// <summary>Show a new notification.</summary>
		public static void Show(Notification notification) {
			Instance.board.Show(notification);
		}

		/// <summary>Hide a visible notification.</summary>
		public static void Hide(Notification notification) {
			Instance.board.Hide(notification);
		}

		protected override void Awake() {
			base.Awake();

			var boardPrefab = Resources.Load<Internal.NotificationsBoard>("UI.NotificationsBoard");
			board = GameObject.Instantiate(boardPrefab);
			board.transform.SetParent(transform);
		}
	}
}
