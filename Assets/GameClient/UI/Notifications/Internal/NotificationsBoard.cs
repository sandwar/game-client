using System;
using UnityEngine;
using System.Collections.Generic;
using SandWar.Unity.Tools;

namespace GameClient.UI.Notifications.Internal
{
	// All public methods are thread safe.
	class NotificationsBoard : MonoBehaviour
	{
		// TODO: what happens if Show() is called on a notification that is being hidden?

		Internal.NotificationMessage messagePrefab;

		#pragma warning disable 0649

		public Transform NotificationsContainer;

		#pragma warning restore 0649

		// NOTE: 'visible' also contains notification that have been requested to be hidden, but are still on the screen.
		readonly Dictionary<Notification, NotificationMessage> visible = new Dictionary<Notification, NotificationMessage>();
		readonly List<Notification> toShow = new List<Notification>();
		readonly List<Notification> toHide = new List<Notification>();

		public void Show(Notification notification) {
			lock (visible) {
				// If already visible we have nothing to do...
				if (toShow.Contains(notification))
					return;

				// ...else if being hidden but not yet...
				if (toHide.Remove(notification))
					return;

				// ...else if notification is already visible...
				if (visible.ContainsKey(notification))
					return;

				// ...else we add it to the list.
				toShow.Add(notification);
			}

			TaskFactory.ScheduleUnity(() => {
				lock (visible) {
					if (toShow.Contains(notification)) {
						toShow.Remove(notification);

						var message = GameObject.Instantiate(messagePrefab);
						message.Initialize(notification, () => {
							GameObject.Destroy(message.gameObject);
							visible.Remove(notification);
						});
						message.transform.SetParent(NotificationsContainer);

						visible.Add(notification, message);
					}

					// else it has been hidden before we reached here.
				}
			});
		}

		public void Hide(Notification notification) {
			lock (visible) {
				// If this notification is to be shown but not visible yet we remove it...
				if (toShow.Remove(notification))
					return;

				// ...else if it is already requested to hide this notification...
				if (toHide.Contains(notification))
					return;

				// ...else if this notification not visible...
				if (!visible.ContainsKey(notification))
					return;
			
				// ...else we add it to the list.
				toHide.Add(notification);
			}

			TaskFactory.ScheduleUnity(() => {
				lock (visible) {
					if (toHide.Contains(notification)) {
						toHide.Remove(notification);

						var message = visible[notification];
						message.Hide();
					}
					
					// else it has been shown again before we reached here.
				}
			});
		}

		void Awake() {
			messagePrefab = Resources.Load<Internal.NotificationMessage>("UI.NotificationMessage");
		}
	}
}
