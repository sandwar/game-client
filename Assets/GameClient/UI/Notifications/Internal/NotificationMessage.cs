using System;
using UnityEngine;
using UnityEngine.UI;
using SandWar.Unity.Tools;
using System.Collections.Generic;

namespace GameClient.UI.Notifications.Internal
{
	class NotificationMessage : MonoBehaviour
	{
		#pragma warning disable 0649

		public Image Icon;
		public Text Message;

		public LayoutElement LayoutElement;
		public Image Image;

		#pragma warning restore 0649
		
		public float OpenSpeed = 100f;
		public float FadeInSpeed = 2f;
		public float WaitTime = 5f;
		public float FadeOutSpeed = 1f;
		public float ShrinkHeightSpeed = 30f;

		object hideNowLock = new object();
		bool hideNow;

		public void Initialize(Notification notification, Action onFinished) {
			var color = Message.color;
			color.a = 0f;
			Message.color = color;
			Message.text = notification.Text;

			var icolor = Icon.color;
			icolor.a = 0f;
			Icon.color = icolor;
			Icon.sprite = notification.Icon;
			if (notification.Icon == null)
				Icon.enabled = false;

			Routine.Start(notificationAnimation(notification.Persistent, onFinished), Routine.InUnity);
		}
		
		public void Hide() {
			lock (hideNowLock)
				hideNow = true;
		}

		IEnumerable<Routine.Operation> notificationAnimation(bool persistent, Action onFinished) {
			// NOTE: for some obscure reason 'Message.rectTransform.rect.size' is [0, 0] if we don't skip a frame.
			yield return Routine.OnNextFrame;

			// Initialize the message...
			float destinationX = Message.rectTransform.rect.size.x + 39;
			LayoutElement.minWidth = 10;
			LayoutElement.minHeight = Message.rectTransform.rect.size.y + 10;
			Message.GetComponent<ContentSizeFitter>().enabled = false;
			
			var color = Icon.color;
			color.a = 1f;
			Icon.color = color;

			yield return Routine.OnNextFrame;

			// ...increment the size of this notification until fully expanded...
			while (LayoutElement.minWidth < destinationX) {
				LayoutElement.minWidth += Time.deltaTime * OpenSpeed;
				
				var localPosition = Icon.rectTransform.localPosition;
				localPosition.x = Mathf.Min (LayoutElement.minWidth - 21, 13);
				Icon.rectTransform.localPosition = localPosition;

				yield return Routine.OnNextFrame;
			}

			// ...increment the opacity of the text until fully visible...
			while (Message.color.a < 1f) {
				color = Message.color;
				color.a += Mathf.Clamp01(Time.deltaTime * FadeInSpeed);
				Message.color = color;

				yield return Routine.OnNextFrame;
			}

			// ...wait 'waitTime' seconds, interrupting if 'hideNow' becomes true...
			//yield return Routine.AfterTime(TimeSpan.FromSeconds(WaitTime));
			float endTime = Time.time + WaitTime;
			do {
				yield return Routine.OnNextFrame;

				lock (hideNowLock)
					if (hideNow)
						break;
			} while (persistent == true || Time.time < endTime);
			
			// ...fade away...
			while (Message.color.a > 0f) {
				var mcolor = Message.color;
				var icolor = Icon.color;
				var tcolor = Image.color;
				mcolor.a -= Time.deltaTime * FadeOutSpeed;
				icolor.a = mcolor.a;
				tcolor.a = mcolor.a;
				Message.color = mcolor;
				Icon.color = icolor;
				Image.color = tcolor;

				yield return Routine.OnNextFrame;
			}

			// ...reduce height until completely flat...
			while (LayoutElement.minHeight > 0f) {
				LayoutElement.minHeight -= Time.deltaTime * ShrinkHeightSpeed;
				LayoutElement.preferredHeight = LayoutElement.minHeight;
			}

			// ...report animation completion.
			onFinished();
		}
	}
}
