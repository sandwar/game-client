using System;
using GameClient.UI.Notifications.Internal;
using UnityEngine.UI;
using System.Threading;
using UnityEngine;
using SandWar.Tools;

namespace GameClient.UI.Notifications
{
	/// <summary>
	/// A notification that can be displayed on the screen.
	/// 
	/// Fully thread safe.
	/// </summary>
	public class Notification
	{
		static TightGUIDGenerator guidGenerator = new TightGUIDGenerator(0, 1, 0);

		readonly object @lock = new object();
		readonly long id = guidGenerator.Next();

		Sprite icon;
		string text;
		bool persistent;

		/// <summary>The icon of this notification.</summary>
		public Sprite Icon {
			get { lock (@lock) return icon; }
			private set { lock (@lock) icon = value; }
		}
		
		/// <summary>The textual content of this notification.</summary>
		public string Text {
			get { lock (@lock) return text; }
			private set { lock (@lock) text = value; }
		}

		/// <summary>Determines if this notification should stay on the screen until dismissed.</summary>
		public bool Persistent {
			get { lock (@lock) return persistent; }
			private set { lock (@lock) persistent = value; }
		}

		public Notification(string text) {
			this.Text = text;
		}

		public Notification(string text, Sprite icon) : this(text) {
			this.Icon = icon;
		}

		public Notification(string text, Sprite icon, bool persistent) : this(text, icon) {
			this.Persistent = persistent;
		}

		/// <summary>Make this notification visible.</summary>
		public void Show() {
			Notifications.Show(this);
		}

		/// <summary>Dismiss this notification.</summary>
		public void Hide() {
			Notifications.Hide(this);
		}

		public override int GetHashCode() {
			return id.GetHashCode();
		}
	}
}

