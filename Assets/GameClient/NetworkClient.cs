using System;
using SandWar.Unity.Tools;
using SandWar.Network;
using SandWar.Common;
using SandWar.Logging;
using SandWar.Unity.Logging;
using GameClient.UI.Notifications;
using UnityEngine;

namespace GameClient
{
	[Singleton.SelfInitialized, Singleton.Persistent]
	public class NetworkClient : Singleton<NetworkClient>
	{
		private NetworkClient() { }

		void Start() {
			Notifications.EnsureInstantiated();

			TaskFactory.Schedule(() => {
				var msg = new Notification("Hello, World! (async)");
				msg.Show();
			});

			/*var client = LidgrenNetwork.CreateClient<PlayerData>(
				ServerProperties.ServerPort,
				new Log(new UnityLog(LogLevel.Warning))
			);
			client.OnConnect = () => {};*/
		}
	}
}

