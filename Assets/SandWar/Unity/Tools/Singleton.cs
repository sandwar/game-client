﻿using UnityEngine;
using System;
using System.Linq;
using SandWar.Tools.Reflection;

namespace SandWar.Unity.Tools
{
	// used internally
	public static class Singleton {
		/// <summary>Forces an instance a <see cref="Singleton" /> to be automatically added to every scene.</summary>
		public class SelfInitializedAttribute : Attribute { }

		/// <summary>Ensures this <see cref="Singleton" /> to persist when changing scene.</summary>
		public class PersistentAttribute : Attribute { }

		/// <summary>Hides the instance of this <see cref="Singleton" /> from the scene.</summary>
		public class HiddenAttribute : Attribute { }

		class FakeSingleton : Singleton<FakeSingleton> { }

		// ensures singletons with SelfInitializedAttribute are initialized
		[RuntimeInitializeOnLoadMethod]
		static void initSingletons() {
			foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies()) {
				foreach (var type in assembly.GetTypes().Where(type => type.IsDefined(typeof(Singleton.SelfInitializedAttribute), false))) {
					//Debug.Log("Force init:" + type.FullName);
					var ensureInstantiated = Finder.GetMethod(() => FakeSingleton.EnsureInstantiated()).GetMethodDefinitionForType(type.BaseType);
					ensureInstantiated.Invoke(null, new object[0]);
				}
			}
		}
	}

	/// <summary>
	/// Provides a way to quickly instantiate singletons.
	/// 
	/// Always seal the inheritor class.
	/// </summary>
	[AddComponentMenu("")]
	public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
	{
		static object @lock = new object();

		static bool instantiated = false;
		static bool quitting = false;
		static T instance = null;

		static bool persistent {
			get { return typeof(T).IsDefined(typeof(Singleton.PersistentAttribute), true); }
		}

		static bool visible {
			get { return !typeof(T).IsDefined(typeof(Singleton.HiddenAttribute), true); }
		}

		/// <summary>Holds a reference to the instance of this <see cref="Singleton" />.</summary>
		protected static T Instance {
			get {
				EnsureInstantiated();
				return instance;
			}
		}

		/// <summary>Intantiates a <see cref="Singleton" /> of type <typeparamref>T</typeparamref> if not already in the scene.</summary>
		public static void EnsureInstantiated() {
			lock (@lock) {
				if (!instantiated) {
					if (quitting)
						return;

					instantiated = true;

					instance = GameObject.FindObjectOfType<T>();
				
					if (instance == null) {
						var singleton = new GameObject(typeof(T).FullName + "(Singleton)");
						instance = singleton.AddComponent<T>();

						if (!visible)
							singleton.hideFlags = HideFlags.HideInHierarchy;
					}
				
					if (persistent)
						GameObject.DontDestroyOnLoad(instance.gameObject);
				}
			}
		}

		/// <summary>
		/// Used by Unity to initialize the <see cref="GameObject" />.
		/// 
		/// Always call this method if overriding.
		/// </summary>
		protected virtual void Awake() {
			if (instance != null && instance != this) {
				// Do not log anything, because switching scene triggers this code if there is an instance of the Singleton
				//Debug.LogWarningFormat(this, "Multiple instances of Singleton<{0}> found on the scene.", typeof(T).FullName);
				GameObject.Destroy(gameObject);
			}
		}

		/// <summary>
		/// Used by Unity to destroy the <see cref="GameObject" />.
		/// 
		/// Always call this method if overriding.
		/// </summary>
		protected virtual void OnDestroy() {
			lock (@lock) {
				if (instance == this) {
					instantiated = false;
					instance = null;
				}
			}
		}

		/// <summary>
		/// Called by Unity when the application is quitting.
		/// 
		/// Always call this method if overriding.
		/// </summary>
		protected virtual void OnApplicationQuit() {
			lock (@lock) {
				quitting = true;
			}
		}
	}
}
