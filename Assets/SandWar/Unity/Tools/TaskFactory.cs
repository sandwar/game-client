using System;
using System.Threading;
using SandWar.Tools.Collections.Concurrent;
using UnityEngine;
using System.Collections.Generic;

namespace SandWar.Unity.Tools
{
	/// <summary>Allows to run tasks asynchrounously.</summary>
	[AddComponentMenu("")]
	// SelfInitialized required to avoid error if EnsureInstantiated (called by ScheduleUnity) is called from a non-Unity thread
	[Singleton.SelfInitialized, Singleton.Persistent, Singleton.Hidden]
	public sealed class TaskFactory : Singleton<TaskFactory>
	{
		ConcurrentQueue<Action> unityQueue = new ConcurrentQueue<Action>();

		object nextFrameLock = new object();

		List<Action> unityNextFrameQueue = new List<Action>();
		List<Action> unityNextFrameQueueAlternative = new List<Action>();

		#if UNITY_EDITOR
		static object playmodeStateLock = new object();
		static bool isPlayingOrWillChangePlaymode;
		static bool isPaused;

		static ConcurrentQueue<Action> editorQueue = new ConcurrentQueue<Action>();

		[UnityEditor.InitializeOnLoadMethod]
		static void initEditor() {
			isPlayingOrWillChangePlaymode = UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode;
			isPaused = UnityEditor.EditorApplication.isPaused;
			UnityEditor.EditorApplication.playmodeStateChanged += () => {
				lock (playmodeStateLock) {
					isPlayingOrWillChangePlaymode = UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode;
					isPaused = UnityEditor.EditorApplication.isPaused;
				}
			};

			UnityEditor.EditorApplication.update += () => {
				Action action;
				int max = editorQueue.Count * 2;
				while (--max > 0 && editorQueue.TryDequeue(out action)) {
					try {
						action();
					} catch (Exception e) {
						Debug.LogException (e);
					}
				}
			};
		}
		#endif

		private TaskFactory() { }

		/// <summary>Schedule an action to be run in Unity context.</summary>
		public static void ScheduleUnity(Action action) {
			Instance.unityQueue.Enqueue(action);
		}

		/// <summary>Schedule an action to be run in Unity context.</summary>
		/// <param name="nextFrame">Ensures the action to not be executed in the same frame as the current one.</param>
		public static void ScheduleUnity(Action action, bool nextFrame) {
			if (nextFrame) {
				lock (Instance.nextFrameLock)
					Instance.unityNextFrameQueue.Add(action);
			} else {
				ScheduleUnity(action);
			}
		}

		/// <summary>
		/// Schedule an action to be run in Unity editor context.
		/// 
		/// Only valid when running Unity in the editor, has no effect in built games.
		/// </summary>
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void ScheduleEditor(Action action) {
			#if UNITY_EDITOR
			editorQueue.Enqueue(action);
			#endif
		}

		/// <summary>Schedule an action to be run on the first available CPU.</summary>
		public static void Schedule(Action action) {
			#if UNITY_EDITOR
			lock (playmodeStateLock) {
				if (!isPlayingOrWillChangePlaymode)
					return;

				if (isPaused) {
					ScheduleUnity(() => Schedule(action));
					return;
				}
			}
			#endif

			ThreadPool.QueueUserWorkItem(state => {
				try {
					action();
				} catch(Exception e) {
					Debug.LogException(e);
				}
			});
		}

		/// <summary>
		/// Schedule an action to be run on the first available CPU, never running
		/// concurrently with other actions scheduled with the same <paramref name="lock" />.
		/// </summary>
		public static void Schedule(Action action, object @lock) {
			#if UNITY_EDITOR
			lock (playmodeStateLock) {
				if (!isPlayingOrWillChangePlaymode)
					return;

				if (isPaused) {
					ScheduleUnity(() => Schedule(action, @lock));
					return;
				}
			}
			#endif

			ThreadPool.QueueUserWorkItem(state => {
				try {
					lock (@lock) {
						action();
					}
				} catch(Exception e) {
					Debug.LogException(e);
				}
			});
		}

		void Update() {
			// Run actions in unityNextFrameQueue.
			lock (Instance.nextFrameLock) {
				var buff = unityNextFrameQueue;
				unityNextFrameQueue = unityNextFrameQueueAlternative;
				unityNextFrameQueueAlternative = buff;
			}

			foreach (var action in unityNextFrameQueueAlternative)
				action();

			unityNextFrameQueueAlternative.Clear();

			// Run actions in unityQueue.
			// The limit is to prevent freezing with actions that add other actions.
			Action action2;
			int max = unityQueue.Count * 2;
			while (--max > 0 && unityQueue.TryDequeue(out action2)) {
				try {
					action2();
				} catch (Exception e) {
					Debug.LogException (e);
				}
			}
		}
	}
}
