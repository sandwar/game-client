using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandWar.Unity.Tools
{
	/// <summary>Allows to execute code routines distributed among the available CPUs.</summary>
	public static class Routine
	{
		/// <summary>Describes an operation in a routine.</summary>
		public struct Operation {
			/// <summary>The context of execution of this operation.</summary>
			public readonly Context Context;
			/// <summary>The end time, if a *WithDelay operation.</summary>
			public readonly float EndTime;

			public Operation(Context context) {
				Context = context;
				EndTime = default(float);
			}

			public Operation(Context context, float endTime) {
				Context = context;
				EndTime = endTime;
			}
		}

		/// <summary>Describes the context of execution of a routine.</summary>
		public enum Context {
			Unknown,
			Synchronous,
			UnityContext,
			UnityContextNextFrame,
			UnityContextWithDelay,
			AnyContext,
			EditorContext
		}

		/// <summary>Execute following code in Unity context.</summary>
		public static readonly Operation InUnity = new Operation(Context.UnityContext);

		/// <summary>Execute following code in Unity context on next frame.</summary>
		public static readonly Operation OnNextFrame = new Operation(Context.UnityContextNextFrame);

		/// <summary>Execute following code on the first available CPU.</summary>
		public static readonly Operation Anywhere = new Operation(Context.AnyContext);

		/// <summary>
		/// Execute following code in Unity editor.
		/// 
		/// Only valid when running Unity in the editor, runs in Unity in built games.
		/// </summary>
		public static readonly Operation InEditor = new Operation(Context.EditorContext);

		/// <summary>Wait a specific amount of time before executing following code in Unity context.</summary>
		public static Operation AfterTime(TimeSpan time) {
			return new Operation(Context.UnityContextWithDelay, Time.time + (float)time.TotalSeconds);
		}

		/// <summary>Start a routine executing the first portion of code synchronously.</summary>
		public static void Start(IEnumerable<Operation> routine) {
			Start(routine, new Operation(Context.Synchronous));
		}

		/// <summary>Start a routine executing the first portion of code in the specified context.</summary>
		public static void Start(IEnumerable<Operation> routine, Operation context) {
			runOne(routine.GetEnumerator(), context);
		}

		static void runOne(IEnumerator<Operation> enumerator, Operation current) {
			if (!enumerator.MoveNext()) {
				enumerator.Dispose();
				return;
			}

			var operation = enumerator.Current;
			if (current.Context == operation.Context &&
			    current.Context != Context.Unknown &&
			    current.Context != Context.UnityContextNextFrame &&
			    current.Context != Context.UnityContextWithDelay
			) {
				runOne(enumerator, current);
				return;
			}

			switch (operation.Context) {
			case Context.AnyContext:
				TaskFactory.Schedule(() => runOne(enumerator, operation));
				break;
			#if UNITY_EDITOR
			case Context.EditorContext:
				TaskFactory.ScheduleEditor(() => runOne(enumerator, operation));
				break;
			#endif
			case Context.Synchronous:
				runOne(enumerator, operation);
				return;
			case Context.UnityContext:
				TaskFactory.ScheduleUnity(() => runOne(enumerator, operation));
				break;
			case Context.UnityContextNextFrame:
				TaskFactory.ScheduleUnity(() => runOne(enumerator, operation), true);
				break;
			case Context.UnityContextWithDelay:
				Action action;
				action = () => {
					// 'action' runs this code [C] on next frame...
					TaskFactory.ScheduleUnity(() => {
						// ...if Time.time did not pass EndTime: calls self (executing [C] on next frame)...
						if (Time.time < operation.EndTime)
							action();
						else
							// ...if it did: executes the next operation.
							runOne(enumerator, operation);
					}, true);
				};

				if (current.Context != Context.UnityContext &&
				    current.Context != Context.UnityContextNextFrame &&
				    current.Context != Context.UnityContextWithDelay)
					TaskFactory.ScheduleUnity(() => {
						// To avoid Time.time not usable from non-Unity threads we run in Unity context if not already...
						action();
					});
				else
					// ...if already in Unity context we are fine.
					action();
				break;
			
			default:
			//case Context.UnityContext:
				Debug.LogWarningFormat("Running in \"{0}\" context is not supported, will run in Unity context.", operation.Context);
				TaskFactory.ScheduleUnity(() => runOne(enumerator, operation));
				break;
			}
		}
	}
}

