using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SandWar.Unity.Tools
{
	/// <summary>Allows to generate previews for <see cref="Transform" />s.</summary>
	[AddComponentMenu("")]
	[Singleton.Persistent, Singleton.Hidden]
	public sealed class ModelPreview : Singleton<ModelPreview>
	{
		static readonly int TemporaryLayer = 31;
		
		Camera myCamera;
		Light myLight;

		private ModelPreview() { }

		/// <summary>Render the preview of a model to a <see cref="Texture" />.</summary>
		/// <param name="model">The model to generate the preview of.</param>
		/// <param name="width">The width of the <see cref="Texture" /> to generate.</param>
		/// <param name="height">The height of the <see cref="Texture" /> to generate.</param>
		public static RenderTexture Render(Transform model, int width, int height) {
			var texture = new RenderTexture(width, height, 16) {
				name = model.name.Substring(0, model.name.Length),
				wrapMode = TextureWrapMode.Clamp,
				antiAliasing = 4
			};
			Render(model, texture);
			return texture;
		}

		/// <summary>Render the preview of a model to a <see cref="RenderTexture" />.</summary>
		/// <param name="model">The model to generate the preview of.</param>
		/// <param name="texture">The <see cref="RenderTexture" /> to render to.</param>
		public static void Render(Transform model, RenderTexture texture) {
			var myModel = Object.Instantiate(model);
			Instance.renderModel(myModel, texture);
			Object.Destroy(myModel.gameObject);
		}

		void renderModel(Transform model, RenderTexture texture) {
			var lights = new List<Light> ();
			foreach (var light in GameObject.FindObjectsOfType<Light>())
				if (light.enabled)
					lights.Add(light);
			
			foreach (var light in lights)
				light.enabled = false;

			model.SetParent(transform);
			model.localPosition = Vector3.zero;
			model.localRotation = Quaternion.identity;
			model.localScale = Vector3.one;
			model.gameObject.layer = TemporaryLayer;
			
			var bounds = new Bounds();
			foreach (var renderer in model.GetComponentsInChildren<Renderer>().Concat(model.GetComponents<Renderer>())) {
				bounds.Encapsulate(renderer.bounds);
				renderer.gameObject.layer = TemporaryLayer;
			}
			
			var dimension = Vector3.Distance(bounds.min, bounds.max);
			var scale = 1f / dimension;
			
			model.localPosition = -(bounds.center * scale);
			model.localScale = Vector3.one * scale;

			myCamera.transform.LookAt(Vector3.zero);

			myCamera.enabled = true;
			myLight.enabled = true;
			myCamera.targetTexture = texture;
			myCamera.Render();
			myCamera.targetTexture = null;
			myLight.enabled = false;
			myCamera.enabled = false;
			
			foreach (var light in lights)
				light.enabled = true;
		}

		protected override void Awake() {
			myCamera = new GameObject("Camera").AddComponent<Camera>();
			myCamera.transform.SetParent(transform);
			myCamera.transform.localPosition = new Vector3(5f, 2f, 3f);
			myCamera.cullingMask = 1 << TemporaryLayer;
			myCamera.useOcclusionCulling = false;
			myCamera.backgroundColor = new Color(0f, 0f, 0f, 0f);
			myCamera.clearFlags = CameraClearFlags.SolidColor;
			myCamera.fieldOfView = 8.5f;
			myCamera.enabled = false;
			myCamera.hideFlags = HideFlags.HideInHierarchy;
			
			myLight = new GameObject("Light").AddComponent<Light>();
			myLight.transform.SetParent(transform);
			myLight.transform.localPosition = new Vector3(3f, 1.5f, -1.5f);
			myLight.cullingMask = 1 << TemporaryLayer;
			myLight.enabled = false;
			myLight.hideFlags = HideFlags.HideInHierarchy;
		}
	}
}

