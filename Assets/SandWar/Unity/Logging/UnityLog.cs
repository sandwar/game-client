using SandWar.Logging;
using UnityEngine;

namespace SandWar.Unity.Logging
{
	public sealed class UnityLog : AbstractLog
	{
		public UnityLog(LogLevel minimumSeverity) : base(minimumSeverity) { }
		
		public UnityLog() : base(LogLevel.Debug) { }

		protected override void Log(LogLevel level, string message, object obj) {
			var unityObj = obj as Object;
			switch (level) {
			case LogLevel.Error:
			case LogLevel.Fatal:
				if (unityObj != null)
					UnityEngine.Debug.LogError(message, unityObj);
				else
					UnityEngine.Debug.LogError(message);
				break;
			case LogLevel.Warning:
				if (unityObj != null)
					UnityEngine.Debug.LogWarning(message, unityObj);
				else
					UnityEngine.Debug.LogWarning(message);
				break;
			default:
				if (unityObj != null)
					UnityEngine.Debug.Log(message, unityObj);
				else
					UnityEngine.Debug.Log(message);
				break;
			}

			var exception = obj as System.Exception;
			if (exception != null)
				UnityEngine.Debug.LogException(exception);
		}
	}
}
