﻿using UnityEngine;
using System.Collections;

namespace SandWar.Unity.Simple.Animation {

	/// <summary>Rotate a <see cref="GameObject" /> constantly.</summary>
	[AddComponentMenu("Animation/Rotation")]
	public class Rotation : MonoBehaviour {

		/// <summary>Rotation speed in degrees/second./summary>
		public Vector3 Speed = new Vector3(0f, 45f, 0f);
		
		void Update() {
			transform.Rotate(Speed * Time.deltaTime);
		}
	}
}
