using SandWar.Math;

namespace SandWar.Unity.Math
{
	public static class TypeConversions
	{
		public static UnityEngine.Vector3 ToUnity(this Vector3 v) {
			return new UnityEngine.Vector3(v.X, v.Y, v.Z);
		}

		public static Vector3 ToSand(this UnityEngine.Vector3 v) {
			return new Vector3(v.x, v.y, v.z);
		}
	}
}
