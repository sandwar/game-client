﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class EditorUtilities : MonoBehaviour { }

#if UNITY_EDITOR

[CustomEditor(typeof(EditorUtilities))]
public class EditorUtilitiesEditor : Editor 
{
	public override void OnInspectorGUI() {
		var transform = ((EditorUtilities)target).transform;

		var lookAt = (Transform)EditorGUILayout.ObjectField("LookAt", null, typeof(Transform), true);
		if (lookAt != null) {
			transform.LookAt(lookAt);
			EditorUtility.SetDirty(transform);
		}
	}
}

#endif