﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Debug/Detach on Destroy")]
public class DetachOnDestroy : MonoBehaviour {

	public Transform[] ToDetach;

	void OnDestroy() {
		foreach (var transform in ToDetach)
			transform.SetParent(null);
	}
}
