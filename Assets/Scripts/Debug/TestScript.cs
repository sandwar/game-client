﻿using UnityEngine;
using SandWar.Unity.Tools;

public class TestScript : MonoBehaviour {

	public Transform TestModel;
	public Texture Result;

	void Start() {
		Result = ModelPreview.Render(TestModel, 256, 256);
	}
}
