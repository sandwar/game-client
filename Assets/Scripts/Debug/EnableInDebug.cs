﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Debug/Enable in Debug Builds")]
public class EnableInDebug : MonoBehaviour {

	public MonoBehaviour ToEnable;
	public bool ShowInEditor = false;
	public bool ShowInDebug = true;
	public bool ShowInRelease = false;

	void Awake() {
		#if UNITY_EDITOR
		ToEnable.enabled = ShowInEditor;
		#else
		ToEnable.enabled = Debug.isDebugBuild ? ShowInDebug : ShowInRelease;
		#endif
	}
}
