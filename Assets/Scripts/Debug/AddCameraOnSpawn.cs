﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Debug/Add Camera on Spawn")]
public class AddCameraOnSpawn : MonoBehaviour {
	
	public bool EnableOnAttach = true;
	public bool DisableOnDetach = false;

	Camera attachedCamera;
	bool quitting;

	void Awake() {
		attachedCamera = Camera.main;

		if (attachedCamera == null)
			attachedCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

		if (attachedCamera == null)
			Debug.LogError("Main camera not available.");

		attachedCamera.transform.SetParent(transform);
		attachedCamera.transform.localPosition = Vector3.zero;
		attachedCamera.transform.localRotation = Quaternion.identity;

		if (EnableOnAttach && !attachedCamera.enabled)
			attachedCamera.enabled = true;
	}

	void OnApplicationQuit() {
		quitting = true;
	}

	void OnDestroy() {
		if (attachedCamera != null && !quitting) {
			if (DisableOnDetach && attachedCamera.enabled)
				attachedCamera.enabled = false;

			attachedCamera.transform.SetParent(null);
			attachedCamera = null;
		}
	}
}
