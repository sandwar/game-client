﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Debug/Mouse Rotation")]
public class MouseRotation : MonoBehaviour {

	public float Multiplier = 180f;
	public bool EnableHorizontal = true;
	public bool EnableVertical = true;

	Vector3 initial;

	void Start() {
		initial = transform.localEulerAngles;
	}

	void Update() {
		float x = Input.GetAxis("Mouse X");
		float y = Input.GetAxis("Mouse Y");

		var eulerAngles = transform.localEulerAngles;
		transform.localEulerAngles = new Vector3(
			EnableVertical ? clampVerticalAngle(eulerAngles.x - y * Time.deltaTime * Multiplier, y < 0) : initial.x,
			EnableHorizontal ? eulerAngles.y + x * Time.deltaTime * Multiplier : initial.y,
			initial.z
		); 
	}

	static float clampVerticalAngle(float angle, bool movingDown) {
		if (movingDown) {
			if (angle > 85f && angle < 270f)
				return 85f;
		} else {
			if (angle > 90f && angle < 275f)
				return 275f;
		}

		return angle;
	}
}
