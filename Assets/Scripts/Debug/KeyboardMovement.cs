﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Debug/Keyboard Movement")]
public class KeyboardMovement : MonoBehaviour {

	public float Multiplier = 1f;

	void Update() {
		float x = Input.GetAxis("Horizontal");
		float z = Input.GetAxis("Vertical");

		transform.Translate(new Vector3(x, 0f, z) * Multiplier);
	}
}
