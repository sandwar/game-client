﻿using UnityEngine;
using System.Collections;
using System;

[AddComponentMenu(""), RequireComponent(typeof(NetworkEntity), typeof(SphereCollider))]
public class PickupEntity : MonoBehaviour {

	public float MaxPickupDistance = 3f;

	public Transform Model;

	SphereCollider SphereCollider;

	[NonSerialized]
	public NetworkEntity NetworkEntity;

	PlayerInventory canPickup = null;

	public void Initialize(Transform model) {
		model.SetParent(Model);
		model.localPosition = Vector3.zero;
		model.localRotation = Quaternion.identity;
	}

	void Awake() {
		SphereCollider = GetComponent<SphereCollider>();
		SphereCollider.radius = MaxPickupDistance;

		NetworkEntity = GetComponent<NetworkEntity>();
	}

	public void Pickup() {
		NetworkEntity.CollectPickup();
	}

	void OnTriggerEnter(Collider other) {
		//Debug.LogFormat("OnTriggerEnter");

		var player = other.GetComponent<PlayerInventory>();
		if (player == null) return;

		canPickup = player;
		player.PickupInRange(this);
	}

	void OnTriggerExit(Collider other) {
		//Debug.LogFormat("OnTriggerExit");

		var player = other.GetComponent<PlayerInventory>();
		if (player == null) return;
		
		canPickup = null;
		player.PickupOutOfRange(this);
	}

	void OnDestroy() {
		//Debug.LogFormat("OnDestroy canPickup={0}", canPickup);

		if (canPickup != null)
			canPickup.PickupOutOfRange(this);
	}
}
