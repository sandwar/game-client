﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameClient.UI.Notifications;

[AddComponentMenu(""), RequireComponent(typeof(NetworkEntity), typeof(PlayerCharacter))]
public class PlayerInventory : MonoBehaviour {

	public Sprite PickupIcon;

	const float MaxPickupDistance = 3;

	public void UpdateInventory() {
		// TODO: show inventory somewhere
		Debug.Log("Inventory update requested!");
		//NetworkEntity.PlayerData.Inventory;
	}
	
	PickupEntity lookingAtPickup;
	Notification pickupNotification;

	void Update() {
		float lowestDistanceSquare = float.PositiveInfinity;
		PickupEntity closestPickup = null;
		
		var camera = Camera.main;
		var cursor = new Vector3(Screen.width / 2f, Screen.height / 2f, 0f);
		foreach(var pickup in pickuppable) {
			var position = camera.WorldToScreenPoint(pickup.Model.position);
			var distanceSquare = (position - cursor).sqrMagnitude;

			if (position.x < 0 || position.y < 0 || position.x > Screen.width || position.y > Screen.height)
				continue;

			if (distanceSquare < lowestDistanceSquare) {
				lowestDistanceSquare = distanceSquare;
				closestPickup = pickup;
			}
		}

		if (closestPickup != null) {
			if (closestPickup != lookingAtPickup) {
				if (lookingAtPickup != null)
					pickupNotification.Hide();

				lookingAtPickup = closestPickup;
				pickupNotification = new Notification(
					string.Format ("Press [{0}] to pick-up {1}.", InputKeys.Pickup, lookingAtPickup.NetworkEntity.PickupData.Model),
					PickupIcon,
					true
				);
				pickupNotification.Show();
			}
		} else {
			if (lookingAtPickup != null) {
				lookingAtPickup = null;
				pickupNotification.Hide();
			}
		}

		if (lookingAtPickup != null && Input.GetKeyDown(InputKeys.Pickup))
			lookingAtPickup.Pickup();
	}

	HashSet<PickupEntity> pickuppable = new HashSet<PickupEntity>();

	public void PickupInRange(PickupEntity pickup) {
		pickuppable.Add(pickup);
	}

	public void PickupOutOfRange(PickupEntity pickup) {
		//Debug.LogFormat("PlayerInventory.PickupOutOfRange hasKey={0}", pickuppable.ContainsKey(pickup));

		pickuppable.Remove(pickup);

		if (lookingAtPickup == pickup) {
			lookingAtPickup = null;
			pickupNotification.Hide();
		}
	}

	void OnDestroy() {
		if (lookingAtPickup != null)
			PickupOutOfRange(lookingAtPickup);

		pickuppable.Clear();
	}
}
