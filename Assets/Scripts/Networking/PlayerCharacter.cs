using UnityEngine;
using System;
using SandWar.Common.Entities;
using SandWar.Common;
using SandWar.Unity.Math;

[AddComponentMenu("Network/Player Character")]
[RequireComponent(typeof(NetworkEntity), typeof(PlayerInventory))]
public class PlayerCharacter : MonoBehaviour
{
	[NonSerialized]
	public PlayerInventory Inventory;

	[NonSerialized]
	public NetworkEntity NetworkEntity;

	public Transform CameraTransform;

	void Awake() {
		Inventory = GetComponent<PlayerInventory>();
		NetworkEntity = GetComponent<NetworkEntity>();
	}
	
	Vector3 lastPosition;
	Vector3 lastLookingAt;
	void Update() {
		var newPosition = transform.position;
		if(newPosition != lastPosition) {
			lastPosition = newPosition;
			NetworkEntity.TransformData.Location = newPosition.ToSand();
			NetworkEntity.PlayerWalk();
		}

		var newLookingAt = CameraTransform.forward;
		if(newLookingAt != lastLookingAt) {
			lastLookingAt = newLookingAt;
			NetworkEntity.CreatureData.LookingAt = newLookingAt.ToSand();
			NetworkEntity.LookAt();
		}

		if (Input.GetKeyDown(InputKeys.PrimaryAction))
			NetworkEntity.PerformAction(ActionType.ActionStart, Key.PrimaryAction);
		if (Input.GetKeyUp(InputKeys.PrimaryAction))
			NetworkEntity.PerformAction(ActionType.ActionEnd, Key.PrimaryAction);

		if (Input.GetKeyDown(InputKeys.SecondaryAction))
			NetworkEntity.PerformAction(ActionType.ActionStart, Key.SecondaryAction);
		if (Input.GetKeyUp(InputKeys.SecondaryAction))
			NetworkEntity.PerformAction(ActionType.ActionEnd, Key.SecondaryAction);
	}
}
