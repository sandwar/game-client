﻿using UnityEngine;
using System.Collections;
using System;

[AddComponentMenu("Network/Creature Character")]
public class CreatureCharacter : MonoBehaviour {

	[NonSerialized]
	public NetworkEntity NetworkEntity;

	public Transform EyesTransform;

	void Awake() {
		NetworkEntity = GetComponent<NetworkEntity>();
	}

	public void SetLookingAt(Vector3 direction) {
		EyesTransform.LookAt(EyesTransform.position + direction);
	}
}
