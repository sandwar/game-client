using SandWar.Common.Collections;
using SandWar.Common.Entities;

public static class Entities
{
	static readonly Entities<EntityData> entities = new Entities<EntityData>();

	public static void Add(EntityData data) {
		entities.Add(data);
	}

	public static bool Remove(long id) {
		return entities.Remove(id);
	}

	public static T ById<T>(long id) where T : EntityData {
		return entities.ById(id) as T;
	}

	public static void Clear() {
		entities.Clear();
	}
}
