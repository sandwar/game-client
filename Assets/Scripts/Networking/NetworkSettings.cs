using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using SandWar.Common;

[AddComponentMenu("")]
public class NetworkSettings : MonoBehaviour {

	[Serializable]
	public class Models {
		public Transform DebugBoxRed;
		public Transform DebugBoxGreen;
		public Transform DebugBoxBlue;

		public Transform DebugModelPracticeTarget;

		public Transform DebugBulletTracing;
	}
	Dictionary<Model, Transform> prefabs;
	void Awake() {
		prefabs = typeof(Models).GetFields().ToDictionary(
			field => (Model)Enum.Parse(typeof(Model), field.Name),
			field => (Transform)field.GetValue(ModelPrefabs)
		);
	}

	public PickupEntity PickupPrefab;
	public ModelEntity ModelPrefab;
	public BulletEntity BulletPrefab;

	public PlayerCharacter PlayerPrefab;
	public CreatureCharacter EnemyPrefab;

	public Models ModelPrefabs;
	public Transform GetPrefab(Model model) {
		return prefabs[model];
	}
}
