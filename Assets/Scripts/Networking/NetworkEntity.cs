using System;
using System.Collections.Generic;
using SandWar.Common;
using SandWar.Common.Entities;
using SandWar.Network;
using UnityEngine;
using SandWar.Unity.Math;
using SandWar.Unity.Tools;

[AddComponentMenu("Network/Network Entity")]
public class NetworkEntity : MonoBehaviour
{
	[NonSerialized]
	public PlayerCharacter PlayerCharacter;

	[NonSerialized]
	public CreatureCharacter Creature;

	#region Entities Collection

	static readonly Dictionary<long, NetworkEntity> entities = new Dictionary<long, NetworkEntity>();
	public static NetworkEntity ById(long id) {
		NetworkEntity entity;
		lock (entities)
			if (!entities.TryGetValue (id, out entity))
				entity = null;
		return entity;
	}

	IClient<PlayerData> client;

	public EntityData EntityData { get; private set; }
	public TransformData TransformData { get; private set; }
	public CreatureData CreatureData { get; private set; }
	public PlayerData PlayerData { get; private set; }
	public PickupData PickupData { get; private set; }
	public ModelData ModelData { get; private set; }
	
	/// <summary>Registers the entity to be managed by the server.</summary>
	public void RegisterEntity(IClient<PlayerData> client, EntityData data) {
		this.client = client;

		lock (entities) {
			NetworkEntity previous;
			if (entities.TryGetValue(data.ID, out previous))
				TaskFactory.ScheduleUnity(() => GameObject.Destroy(previous.gameObject));
			entities[data.ID] = this;
			EntityData = data;
		}

		TransformData = data as TransformData;
		CreatureData = data as CreatureData;
		PlayerData = data as PlayerData;
		PickupData = data as PickupData;
		ModelData = data as ModelData;

		if (PlayerData != null) PlayerCharacter = GetComponent<PlayerCharacter>();

		if (CreatureData != null) Creature = GetComponent<CreatureCharacter>();

		if (TransformData != null) UpdateTransform();
	}

	public static void DestroyAll() {
		lock (entities) {
			foreach(var kvp in entities)
				UnityEngine.Object.Destroy(kvp.Value.gameObject);
			entities.Clear();
			Entities.Clear();
		}
	}

	#endregion

	public void UpdateTransform() {
		transform.position = new Vector3(TransformData.Location.X, TransformData.Location.Y, TransformData.Location.Z);
		transform.rotation = Quaternion.FromToRotation(Vector3.forward, TransformData.Direction.ToUnity());
	}

	public void UpdateCreature() {
		Creature.SetLookingAt(new Vector3(CreatureData.LookingAt.X, CreatureData.LookingAt.Y, CreatureData.LookingAt.Z));
	}

	public void PlayerWalk() {
		var message = client.CreateMessage();
		message.Write((byte)NetworkCommand.PlayerWalk);
		message.Write(TransformData.Location);
		client.SendMessage(message, true, false);
	}

	public void PerformAction(ActionType type, Key key) {
		var message = client.CreateMessage();
		message.Write((byte)NetworkCommand.PerformAction);
		message.Write((byte)type);
		message.Write((byte)key);
		message.Write(TransformData.Location);
		message.Write(CreatureData.LookingAt);
		client.SendMessage(message, true, true);
	}

	public void LookAt() {
		var message = client.CreateMessage();
		message.Write((byte)NetworkCommand.LookAt);
		message.Write(CreatureData.LookingAt);
		client.SendMessage(message, true, false);
	}

	public void CollectPickup() {
		var message = client.CreateMessage();
		message.Write((byte)NetworkCommand.CollectPickup);
		message.Write(PickupData.ID);
		client.SendMessage(message, true, true);
	}

	void OnDestroy() {
		lock (entities)
			if(EntityData != null)
				entities.Remove(EntityData.ID);
	}
}
