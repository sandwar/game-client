﻿using System.Collections;
using SandWar.Common;
using SandWar.Common.Entities;
using SandWar.Network;
using UnityEngine;
using System.Threading;
using System.Collections.Generic;
using SandWar.Logging;
using SandWar.Unity.Logging;
using SandWar.Unity.Tools;
using GameClient.UI.Notifications;

[AddComponentMenu("Network/Network Client")]
[RequireComponent(typeof(NetworkSettings))]
public class NetworkClient : MonoBehaviour {

	public Sprite ConnectingIcon;
	public Sprite ConnectedIcon;
	public Sprite DisconnectedIcon;

	IClient<PlayerData> client;
	NetworkSettings settings;
	bool connectedOnce = false;

	void Awake() {
		settings = GetComponent<NetworkSettings>();
	}

	void Start() {
		bool connected = false;

		Notification connecting = null;

		client = LidgrenNetwork.CreateClient<PlayerData>(ServerProperties.ServerPort, new Log(new UnityLog(LogLevel.Warning)));
	
		client.OnConnect = () => {
			try {
				new Notification(connectedOnce ? "Connection restored!" : "Connected!", ConnectedIcon).Show();
				connected = true;
				connectedOnce = true;

				if (connecting != null) {
					connecting.Hide();
					connecting = null;
				}
			} catch(System.Exception e) {
				Debug.LogException(e);
			}
		};

		client.OnMessageIncoming = (message) => {
			try {
				//Debug.Log("Message received.");
				processMessage(message);
			} catch(System.Exception e) {
				Debug.LogException(e);
			}
		};

		client.OnDisconnect = () => {
			try {
				new Notification(connected ? "Connection lost!" : "Failed to connect...", DisconnectedIcon).Show();
				connected = false;

				if (connecting != null) {
					connecting.Hide();
					connecting = null;
				}

				TaskFactory.ScheduleUnity(() => {
					NetworkEntity.DestroyAll();
					Start();
				});
			} catch(System.Exception e) {
				Debug.LogException(e);
			}
		};

		connecting = new Notification(connectedOnce ? "Restoring connection..." : "Connecting to server...", ConnectingIcon, true);
		Notifications.Show(connecting);
		client.ConnectAsync(ServerProperties.ServerAddress).Start();
	}

	void OnDestroy() {
		client.Disconnect();
	}

	void processMessage(IMessageIn<PlayerData> message) {
		var command = (NetworkCommand)message.ReadByte();

		//Debug.Log("Command: "+ command);

		switch (command) {
		case NetworkCommand.MessagesBundle:
			var count = message.ReadUnsignedShort();
			//Debug.LogFormat("MessagesBundle, count={0}", count);

			for(ushort c = 0; c < count; c++)
				processMessage(message);
			break;
		
		case NetworkCommand.YourPlayerAdd:
			var data = message.Read<PlayerData>();
			//Debug.LogFormat("YourPlayerAdd, data.ID={0}", data.ID);

			Entities.Add(data);

			client.Data = data;
			TaskFactory.ScheduleUnity(() => Object.Instantiate(settings.PlayerPrefab).NetworkEntity.RegisterEntity(client, data));
			break;
		
		case NetworkCommand.PickupAdd:
			var data3 = message.Read<PickupData>();
			//Debug.LogFormat("PickupAdd, data.ID={0}", data3.ID);

			Entities.Add(data3);

			TaskFactory.ScheduleUnity(() => {
				var e1 = Object.Instantiate(settings.PickupPrefab);
				e1.Initialize(Object.Instantiate(settings.GetPrefab(data3.Model)));
				e1.NetworkEntity.RegisterEntity(client, data3);
			});
			break;
		
		case NetworkCommand.PlayerAdd:
			var data2 = message.Read<PlayerData>();
			//Debug.LogFormat("PlayerAdd, data.ID={0}", data2.ID);

			Entities.Add(data2);
			TaskFactory.ScheduleUnity(() => Object.Instantiate(settings.EnemyPrefab).NetworkEntity.RegisterEntity(client, data2));
			break;
		
		case NetworkCommand.TransformSetLocation:
			var id = message.ReadLong();
			var location = message.Read(new SandWar.Math.Vector3());
			//Debug.LogFormat("TransformSetLocation, id={0}, location={1}", id, location);

			var entity = Entities.ById<TransformData>(id);
			if (entity != null)
				entity.Location = location;
			else
				Debug.LogWarningFormat("Unknown TransformData {0} with command {1}", id, command);

			TaskFactory.ScheduleUnity(() => {
				var uentity = NetworkEntity.ById(id);
				if (uentity != null)
					uentity.UpdateTransform();
				else
					Debug.LogWarningFormat("Unknown NetworkEntity {0} with command {1}", id, command);
			});
			break;
		
		case NetworkCommand.DestroyEntity:
			var id2 = message.ReadLong();
			//Debug.LogFormat("DestroyEntity, id={0}", id2);

			if (!Entities.Remove(id2))
				Debug.LogWarningFormat("Unknown EntityData {0} with command {1}", id2, command);

			TaskFactory.ScheduleUnity(() => {
				var uentity2 = NetworkEntity.ById(id2);
				if (uentity2)
					GameObject.Destroy(uentity2.gameObject);
				else
					Debug.LogWarningFormat("Unknown NetworkEntity {0} with command {1}", id2, command);
			});
			break;
		
		case NetworkCommand.AddToInventory:
			var item = message.Read<ItemData>();

			client.Data.Inventory.Add(item);

			TaskFactory.ScheduleUnity(() => {
				var myCharacter = NetworkEntity.ById(client.Data.ID);
				if (myCharacter)
					myCharacter.PlayerCharacter.Inventory.UpdateInventory();
				else
					Debug.LogWarningFormat("Unknown NetworkEntity {0} with command {1}", id2, command);
			});
			break;
		
		case NetworkCommand.CreatureSetLookingAt:
			var id4 = message.ReadLong();
			var lookingAt = message.Read(new SandWar.Math.Vector3());
			//Debug.LogFormat("TransformSetLocation, id={0}, lookingAt={1}", id4, lookingAt);

			var entity4 = Entities.ById<CreatureData>(id4);
			if (entity4 != null)
				entity4.LookingAt = lookingAt;
			else
				Debug.LogWarningFormat("Unknown CreatureData {0} with command {1}", id4, command);

			TaskFactory.ScheduleUnity(() => {
				var uentity4 = NetworkEntity.ById(id4);
				if (uentity4 != null)
					uentity4.UpdateCreature();
				else
					Debug.LogWarningFormat("Unknown NetworkEntity {0} with command {1}", id4, command);
			});
			break;
		
		case NetworkCommand.ModelAdd:
			var data4 = message.Read<ModelData>();
			//Debug.LogFormat("ModelAdd, data.ID={0}", data4.ID);

			Entities.Add(data4);
			TaskFactory.ScheduleUnity(() => {
				var e1 = Object.Instantiate(settings.ModelPrefab);
				e1.Initialize(Object.Instantiate(settings.GetPrefab(data4.Model)));
				e1.NetworkEntity.RegisterEntity(client, data4);
			});
			break;
		
		case NetworkCommand.BulletAdd:
			var data5 = message.Read<BulletData>();
			//Debug.LogFormat("BulletAdd, data.ID={0}", data5.ID);

			Entities.Add(data5);
			TaskFactory.ScheduleUnity(() => {
				var e1 = Object.Instantiate(settings.BulletPrefab);
				e1.Initialize(Object.Instantiate(settings.GetPrefab(data5.Model)));
				e1.NetworkEntity.RegisterEntity(client, data5);
			});
			break;
			
		default:
			Debug.LogWarningFormat("Unknown network command: {0}", command);
			break;
		}
	}
}
