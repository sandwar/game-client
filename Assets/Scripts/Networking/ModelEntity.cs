﻿using UnityEngine;
using System.Collections;
using System;

[AddComponentMenu(""), RequireComponent(typeof(NetworkEntity))]
public class ModelEntity : MonoBehaviour {
	
	public Transform Model;

	[NonSerialized]
	public NetworkEntity NetworkEntity;
	
	public void Initialize(Transform model) {
		model.SetParent(Model);
		model.localPosition = Vector3.zero;
		model.localRotation = Quaternion.identity;
	}

	void Awake() {
		NetworkEntity = GetComponent<NetworkEntity>();
	}
}
