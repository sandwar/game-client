﻿using UnityEngine;
using System.Collections;

public static class InputKeys {
	
	public static KeyCode Pickup = KeyCode.E;

	public static KeyCode PrimaryAction = KeyCode.Mouse0;
	public static KeyCode SecondaryAction = KeyCode.Mouse1;
}
