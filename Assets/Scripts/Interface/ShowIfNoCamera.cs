﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[AddComponentMenu(""), RequireComponent(typeof(Image))]
public class ShowIfNoCamera : MonoBehaviour {

	Image Image;

	void Awake() {
		Image = GetComponent<Image>();
	}

	void Update() {
		Image.enabled = Camera.main == null;
	}
}
