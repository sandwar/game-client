﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
class DependencyUpdater {

	static readonly string baseRepositoryPath = "../";
	static readonly string dependenciesFolder = "Assets/Plugins/Dependencies";

	static readonly int timeBetweenChecks = 5000;  // milliseconds

	static readonly string[] watchPaths = new [] {
		"GameResources/SandWar.Common/bin/Debug/",
		"GameResources/SandWar.Common/bin/Release/",
		"GameServer/GameServer/bin/Debug/",
		"GameServer/GameServer/bin/Release/",
		"Libraries/SandWar.Database/SandWar.Database/bin/Debug/",
		"Libraries/SandWar.Database/SandWar.Database/bin/Release/",
		"Libraries/SandWar.Logging/SandWar.Logging/bin/Debug/",
		"Libraries/SandWar.Logging/SandWar.Logging/bin/Release/",
		"Libraries/SandWar.Math/SandWar.Math/bin/Debug/",
		"Libraries/SandWar.Math/SandWar.Math/bin/Release/",
		"Libraries/SandWar.Network/SandWar.Network/bin/Debug/",
		"Libraries/SandWar.Network/SandWar.Network/bin/Release/",
		"Libraries/SandWar.Serialization/SandWar.Serialization/bin/Debug/",
		"Libraries/SandWar.Serialization/SandWar.Serialization/bin/Release/",
		"Libraries/SandWar.Tools/SandWar.Tools/bin/Debug/",
		"Libraries/SandWar.Tools/SandWar.Tools/bin/Release/",
	};


	static readonly string[] dependencies = new [] {
		"fastJSON.dll",
		"Lidgren.Network.dll",
		"protobuf-net.dll",
		"SandWar.Common.dll",
		"SandWar.Logging.dll",
		"SandWar.Math.dll",
		"SandWar.Network.dll",
		"SandWar.Serialization.dll",
		"SandWar.Tools.dll"
	};

	// -- END OF SETTINGS, ACTUAL CODE NOW --

	struct DllStatus {
		public DateTime LastWrite;
		public string Path;
	}
	
	static DependencyUpdater() {
		bool update = false;
		object updateLock = new object();
		EditorApplication.update += () => {
			lock (updateLock) {
				if (update) {
					update = false;
					AssetDatabase.Refresh();
				}
			}
		};

		baseRepositoryPath = Path.GetFullPath(baseRepositoryPath);
		for (var i = 0; i < watchPaths.Length; i++)
			watchPaths[i] = Path.Combine(baseRepositoryPath, watchPaths[i]);

		new Thread(() => {
			//Debug.Log("Dependency updater working...");

			try {
				Dictionary<string, DateTime> dllsInUnity = new Dictionary<string, DateTime>();
				Dictionary<string, DllStatus> dlls = new Dictionary<string, DllStatus>();

				while (true) {
					Thread.Sleep(timeBetweenChecks);
					//Debug.Log("Checking for DLL updates...");

					// check current DLL versions
					foreach (var dependency in dependencies) {
						var path = Path.Combine(dependenciesFolder, dependency);
						if (!File.Exists(path)) continue;

						dllsInUnity[dependency] = File.GetLastWriteTimeUtc(path);
					}

					// get newest DLLs from watch paths
					foreach (var dependency in dependencies) {
						DateTime lastWrite = DateTime.MinValue;
						string newestPath = null;

						foreach (var watchPath in watchPaths) {
							var path = Path.Combine(watchPath, dependency);
							if (!File.Exists(path)) continue;

							var thisLastWrite = File.GetLastWriteTimeUtc(path);
							if (thisLastWrite > lastWrite) {
								lastWrite = thisLastWrite;
								newestPath = path;
							}
						}

						if (newestPath != null) {
							dlls[dependency] = new DllStatus {
								LastWrite = lastWrite,
								Path = newestPath
							};
						}
					}

					// update any obsolete version
					bool updated = false;
					foreach (var kvp in dlls) {
						var dllName = kvp.Key;
						var dll = kvp.Value;

						DateTime unityVersion;
						if (!dllsInUnity.TryGetValue(dllName, out unityVersion))
							unityVersion = DateTime.MinValue;

						if (unityVersion < dll.LastWrite) {
							updated = true;
							File.Copy(dll.Path, Path.Combine(dependenciesFolder, dllName), true);
						}
					}

					// TODO: do not update until there are no modifications for two consecutive seconds
					// TODO: do not update until Unity editor is restored from background
					if (updated)
						lock (updateLock)
							update = true;

					// clear dictionaries for the next iteration
					dllsInUnity.Clear();
					dlls.Clear();
				}
			} catch (Exception e) {
				Debug.LogException(e);
			}
		}).Start();
	}
}

#endif
